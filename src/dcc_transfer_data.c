#include "dcc_transfer_data.h"

struct dcc_transfer dcc_transfer_one()
{
	struct dcc_transfer transfer;
	transfer.length = 1;
	transfer.type=DCC_TRANSFER_ONES;
	return transfer;
}

struct dcc_transfer dcc_transfer_zero()
{
	struct dcc_transfer transfer;
	transfer.length = 1;
	transfer.type=DCC_TRANSFER_ZEROS;
	return transfer;
}

struct dcc_transfer dcc_transfer_ones(int number)
{
	struct dcc_transfer transfer;
	transfer.length = number;
	transfer.type=DCC_TRANSFER_ONES;
	return transfer;
}
struct dcc_transfer dcc_transfer_zeros(int number)
{
	struct dcc_transfer transfer;
	transfer.length = number;
	transfer.type=DCC_TRANSFER_ZEROS;
	return transfer;
}

struct dcc_transfer dcc_transfer_buffer(unsigned char* buffer,int size)
{
	struct dcc_transfer transfer;
	transfer.length = size;
	transfer.buffer=buffer;
	transfer.type=DCC_TRANSFER_BITS;
	return transfer;
}

struct dcc_transfer dcc_transfer_byte(unsigned char* buffer)
{
	struct dcc_transfer transfer;
	transfer.length = 8;
	transfer.buffer=buffer;
	transfer.type=DCC_TRANSFER_BITS;
	return transfer;
}
