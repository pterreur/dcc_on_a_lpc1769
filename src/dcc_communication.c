//#include "dcc_transmitter.h"
//
//int main (void)
//{
//    dcc_transmitter_initialize();
//    dcc_communication_set_speed(0xAA,1);
//
//    while(1) {
//        //do nothing
//    }
//    return 0;
//
//}
//
//
//unsigned char speed_traps[] = {
//    0x02,0x12,0x03 //TODO: complete the list of speedtraps
//};
//
//#define MAX_SPEED_TRAP        sizeof(speed_traps)/sizeof(unsigned char) + 1
//#define BASE_INSTRUCTION    0x40
//
//struct dcc_transfer transfers[8];
//unsigned char transfer_instruction;
//unsigned char transfer_adress;
//
//int dcc_communication_set_speed(unsigned char adress,unsigned char speed_trap) {
//
//    if (speed_trap <= MAX_SPEED_TRAP) {
//        transfer_instruction = speed_traps[speed_trap-1] | (unsigned char)BASE_INSTRUCTION;
//        transfer_adress = adress;
//
//        transfers[0] = dcc_transfer_ones(12);
//        transfers[1] = dcc_transfer_zero();
//        transfers[2] = dcc_transfer_byte(&transfer_adress);
//        transfers[3] = dcc_transfer_zero();
//        transfers[4] = dcc_transfer_byte(&transfer_instruction);
//        transfers[5] = dcc_transfer_zero();
//        transfers[6] = dcc_transfer_ones(8);//checksum?
//        transfers[7] = dcc_transfer_one();
//
//        dcc_transmitter_send(transfers,10);
//        return 0;
//    }
//
//    return -1;
//
//}

//#include "dcc_transmitter.h"
//
//int main (void)
//{
//	dcc_transmitter_initialize();
//
//	struct dcc_transfer transfers[8];
//
//	unsigned char adress=0x03;
//	unsigned char instruction=0x74;
//	unsigned char checksum=adress^instruction;
//
//
//	//functies te maken (speedtrap, direction), bevatten
//void
//
//////////////////////////////////////////////////////////////////////////////////////////////////
//	//enkele basiscommandos
//	//ADRES
//	//0X03		= node 3
//	//INSTRUCTIE 01DC SSSS
//	//0100 0000	= basis snelheid richting
//	//0110 0010 = speedtrap 1, voorwaarts
//	//0100 0010 = speedtrap 1, achterwaarts
//	//0111 1111 = speedtrap 28, voorwaarts
//	//0101 1111 = speedtrap 28, achterwaarts
//////////////////////////////////////////////////////////////////////////////////////////////////
//
//	transfers[0] = dcc_transfer_ones(14);
//	transfers[1] = dcc_transfer_zero();
//	transfers[2] = dcc_transfer_byte(&adress);
//	transfers[3] = dcc_transfer_zero();
//	transfers[4] = dcc_transfer_byte(&instruction);
//	transfers[5] = dcc_transfer_zero();
//	transfers[6] = dcc_transfer_byte(&checksum);//checksum?
//	transfers[7] = dcc_transfer_one();
//
//	dcc_transmitter_send(transfers,8);
//
//    while(1) {
//        //do nothing
//    }
//    return 0;
//
//}
///////////////////////////////////////////////////////////////////////////////////////////////
#include "dcc_transmitter.h"
#include "dcc_communication.h"

static struct dcc_transfer transfers[8];

static unsigned char transfer_adress;
static unsigned char transfer_instruction;
//static unsigned char checksum=0xaa;
static unsigned char checksum;

int dcc_communication_send_instruction_byte(unsigned char adress,unsigned char instruction)
{
    transfer_adress = adress;
    transfer_instruction = instruction;

    checksum=adress^(instruction);


    transfers[0] = dcc_transfer_ones(14);
    transfers[1] = dcc_transfer_zero();
    transfers[2] = dcc_transfer_byte(&transfer_adress);
    transfers[3] = dcc_transfer_zero();
    transfers[4] = dcc_transfer_byte(&transfer_instruction);
    transfers[5] = dcc_transfer_zero();
    transfers[6] = dcc_transfer_byte(&checksum);
    transfers[7] = dcc_transfer_ones(2);

    dcc_transmitter_send(transfers,8);

    return 0;
}

//int dcc_communication_send_instruction_buffer(unsigned char adress,unsigned char* instruction,unsigned int instruction_size_in_bits)
//{
//
//    unsigned char checksum=adress^(*instruction);
//
//    transfers[0] = dcc_transfer_ones(12);
//    transfers[1] = dcc_transfer_zero();
//    transfers[2] = dcc_transfer_byte(&adress);
//    transfers[3] = dcc_transfer_zero();
//    transfers[4] = dcc_transfer_buffer(instruction,instruction_size_in_bits);
//    transfers[5] = dcc_transfer_zero();
//    transfers[6] = dcc_transfer_byte(&checksum);//checksum in case of buffer > 8 bits????
//    transfers[7] = dcc_transfer_one();
//
//    dcc_transmitter_send(transfers,8);
//
//    return 0;
//}




