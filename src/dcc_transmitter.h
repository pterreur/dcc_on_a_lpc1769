#ifndef DCC_TRANSMITTER_H_
#define DCC_TRANSMITTER_H_

#include "dcc_transfer_data.h"

void dcc_transmitter_initialize();

void dcc_transmitter_send(struct dcc_transfer buffer[],unsigned int length);

#endif
