////#define MBED_CODE
//
//#include "LPC17xx.h"
//
//#define PCONP                (*((uint32_t *)(0x400FC0C4)))
//
//#ifndef MBED_CODE                 //0.22 => setup LPCXPRESSO
//    #include <cr_section_macros.h>
//
//    #define PINSEL_POS        12
//    #define PINSEL            (*((uint32_t *)(0x4002c004)))
//
//    #define FIOPOS            22
//    #define FIODIR            (*((uint32_t *)(0x2009c000)))
//
//    #define FIOSET            (*((uint32_t *)(0x2009c018)))
//    #define FIOCLR            (*((uint32_t *)(0x2009c01c)))
//
//    #define FIOPIN            (*((uint32_t *)(0x2009C014)))
//#else                          //1.29 => setup MBED
//    #define PINSEL_POS        26
//    #define PINSEL            (*((uint32_t *)(0x4002c00c)))
//
//    #define FIOPOS            20
//    #define FIODIR            (*((uint32_t *)(0x2009c020)))
//
//    #define FIOSET            (*((uint32_t *)(0x2009c038)))
//    #define FIOCLR            (*((uint32_t *)(0x2009c03c)))
//
//    #define FIOPIN            (*((uint32_t *)(0x2009C034)))
//#endif
//
//void configure_gpio()
//{
//    PCONP |= ( 1 << 15 );             		//aanzetten van gpio's
//    PINSEL &= (~(3 << PINSEL_POS));        	//functie van pin bepalen
//
//    FIODIR |= (1 << FIOPOS);
//}
//
//void configure_timer()
//{
//    PCONP |= 1 << 2;               			 //Activatie van timer 1 vanuit power-management-perspectief
//
//    LPC_SC->PCLKSEL0 |= 1 << 4;           	 //PCLK_peripheral = CCLK
//    LPC_TIM1->MR0 = 0x36;           		 //zeer groot getal opdat blink zichtbaar is
//    LPC_TIM1->MCR |= (1 << 0) | (1 << 1);    //reset bij match en activatie interrupt
//    LPC_TIM1->PR = 0xF423F;               		 //voorlopig geen prescaler
//    LPC_TIM1->TCR |= (1 << 1);      	     //reset van de timer
//
//    NVIC_EnableIRQ(TIMER1_IRQn);      		 //irq-activatie
//}
//
//void start_timer()
//{
//    LPC_TIM1->TCR= 1 << 0;            		//aanzetten/enable van de timer
//}
//
//
//int main (void)
//{
//    configure_gpio();
//    configure_timer();
//    start_timer();
//
//    while(1) {
//        //do nothing
//    }
//    return 0;
//
//}
//
//static volatile unsigned char position_of_bit = 0;
//static volatile unsigned char counter = 0;
//static volatile unsigned char toggle_at = 0;
//
//static const unsigned char bits_to_be_transferred[] = {0xff,0x00};
//static const unsigned int number_of_bits=16;
//
//#define CHAR_AT_POSITION(bit)        (*(bits_to_be_transferred + (bit/8)))
//#define BIT_IS_SET_AT(bit)        (CHAR_AT_POSITION(bit)) & (1 << (7-(bit % 8)))
//
//void TIMER1_IRQHandler (void)
//{
//if((LPC_TIM1->IR & 0x01) == 0x01) {
//    LPC_TIM1->IR |= 1 << 0;         //interrupt clearen
//
//    if(counter == 0) {
//        if(BIT_IS_SET_AT(position_of_bit  % number_of_bits)) {
//            counter = 2;
//            toggle_at = 1;
//        } else {
//            counter = 4;
//            toggle_at = 2;
//        }
//        FIOPIN = FIOPIN  | (1 << FIOPOS);
//        position_of_bit++;
//    }
//    if(counter <= toggle_at) {
//        FIOPIN = FIOPIN & ~(1 << FIOPOS);
//    }
//    counter--;
//    }
//}
