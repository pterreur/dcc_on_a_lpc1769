#include "dcc_timer_transmitter_state.h"
#include "dcc_transfer_data.h"

static volatile unsigned int position_of_bit = 0;
static volatile unsigned int position_of_dcc_transfer = 0;
static volatile unsigned char counter = 0;
static volatile unsigned char toggle_at = 0;

static struct dcc_transfer* bits_to_be_transferred;
static unsigned int number_of_transfers;

#define BYTE_POSITION(bit)				(bit/8)
#define BYTE_AT_POSITION(bit,buffer)	(buffer[BYTE_POSITION(bit)])
#define BIT_MASK(bit)					(1 << (7-(bit % 8)))
#define BIT_IS_SET_AT(bit,buffer)		(BYTE_AT_POSITION(bit,buffer)) & BIT_MASK(bit)

void dcc_timer_transmitter_state_load_buffer(struct dcc_transfer* transfers,unsigned int size) {
	position_of_bit = 0;
	position_of_dcc_transfer = 0;
	counter = 0;
	toggle_at = 0;

	bits_to_be_transferred = transfers;
	number_of_transfers=size;
}

void dcc_timer_transmitter_state_reset() {
	position_of_bit = 0;
	position_of_dcc_transfer = 0;
	counter = 0;
	toggle_at = 0;
}

unsigned char is_at_end_of_transfer()
{
	return position_of_bit >= bits_to_be_transferred[position_of_dcc_transfer].length;
}

unsigned char dcc_timer_state_has_next()
{
	if (counter == 0 && is_at_end_of_transfer()) {
		if(position_of_dcc_transfer >= number_of_transfers - 1) {
			return 0;
		}
	}
	return 1;
}


enum dcc_timer_action dcc_timer_state_next()
{
		enum dcc_timer_action result = DCC_DO_NOTHING;
		if(counter == 0) {
			unsigned char bit_is_set=1;

			if(is_at_end_of_transfer()) {
				position_of_dcc_transfer++;
				position_of_bit = 0;
			}

			switch(bits_to_be_transferred[position_of_dcc_transfer].type) {
				case DCC_TRANSFER_BITS:
					bit_is_set = BIT_IS_SET_AT(position_of_bit,bits_to_be_transferred[position_of_dcc_transfer].buffer);
					break;
				case DCC_TRANSFER_ONES:
					bit_is_set = 1;
					break;
				case DCC_TRANSFER_ZEROS:
					bit_is_set = 0;
					break;
			}

			if(bit_is_set) {
				counter = 2;
				toggle_at = 1;
			} else {
				counter = 4;
				toggle_at = 2;
			}

			position_of_bit++;
			result = DCC_TOGGLE_HIGH;
		} 

		if(counter == toggle_at) {
			result = DCC_TOGGLE_LOW;
		}

		counter--;
		return result;
}
