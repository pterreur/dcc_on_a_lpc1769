/*
 * dcc_transfer.h
 *
 *  Created on: Apr 9, 2016
 *      Author: bart
 */

#ifndef DCC_TRANSFER_DATA_H_
#define DCC_TRANSFER_DATA_H_

enum dcc_transfer_type {
	DCC_TRANSFER_ZEROS,
	DCC_TRANSFER_ONES,
	DCC_TRANSFER_BITS
};

struct dcc_transfer {
	enum dcc_transfer_type type;
	unsigned char* buffer;
	unsigned int length;
};

struct dcc_transfer dcc_transfer_one();

struct dcc_transfer dcc_transfer_zero();

struct dcc_transfer dcc_transfer_ones(int number);

struct dcc_transfer dcc_transfer_zeros(int number);

struct dcc_transfer dcc_transfer_buffer(unsigned char* buffer,int size);

struct dcc_transfer dcc_transfer_byte(unsigned char* buffer);

#endif /* DCC_TRANSFER_DATA_H_ */
