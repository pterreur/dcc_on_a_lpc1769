#include "LPC17xx.h"

#include "dcc_transmitter.h"
#include "dcc_timer_transmitter_output.h"
#include "dcc_timer_transmitter_state.h"

////////////////////////////////////////////////////////////
//MACRO VAN MAKEN (tracing)
#define PCONP				(*((uint32_t *)(0x400FC0C4)))
#define PINSEL_POS			10
	#define PINSEL			(*((uint32_t *)(0x4002c004))) //= pinsel1: upper half of Port 0

	#define TESTPIN			21
	#define FIODIR			(*((uint32_t *)(0x2009c000)))

	#define FIOSET			(*((uint32_t *)(0x2009c018)))
	#define FIOCLR			(*((uint32_t *)(0x2009c01c)))

	#define FIOPIN			(*((uint32_t *)(0x2009C014)))

void dcc_timer_transmitter_output_initialize_tracing()
{
    PCONP |= ( 1 << 15 ); 			//aanzetten van gpio's
    PINSEL &= (~(3 << PINSEL_POS));		//functie van pin bepalen

    FIODIR |= (1 << TESTPIN);
}
void timing_output_write_high(){
	FIOPIN = FIOPIN  | (1 << TESTPIN);
}

void timing_output_write_low() {
	FIOPIN = FIOPIN & ~(1 << TESTPIN);
}

//END MACRO
////////////////////////////////////////////////////////////



#define PCONP				(*((uint32_t *)(0x400FC0C4)))

int dcc_out = 0;

static void initialize_timer_1()
{
    PCONP |= 1 << 2;				//Activatie van timer O vanuit power-management-perspectief

    LPC_SC->PCLKSEL0 |= 1 << 4;			//PCLK_peripheral = CCLK
    //LPC_TIM0->MR0 = 1 << 25;			//zeer groot getal opdat blink zichtbaar is
    LPC_TIM0->MCR |= (1 << 0) | (1 << 1);	//reset bij match en activatie interrupt
    LPC_TIM1->PR=0x63;				//enkel voor DCC => concerts 100 MHz to 1 tick/microsec
    LPC_TIM1->MR0=0x3A;					//enkel voor DCC => dec=58
    LPC_TIM0->TCR |= (1 << 1);			//reset van de timer

    NVIC_EnableIRQ(TIMER1_IRQn);		//irq-activatie
}


static void initialize_timer_0()
{
    //TODO

    PCONP |= 1 << 1;				//Activatie van timer 1 vanuit power-management-perspectief

    LPC_SC->PCLKSEL0 |= 1 << 4;			//PCLK_peripheral = CCLK
    //LPC_TIM1->MR0 = 1 << 25;			//zeer groot getal opdat blink zichtbaar is
    LPC_TIM1->MCR |= (1 << 0) | (1 << 1);	//reset bij match en activatie interrupt
    //LPC_TIM1->PR=0x186A0;				//enkel voor DCC => concerts 100 MHz to 1 tick/millisec
    //LPC_TIM1->MR0=0x05;					//enkel voor DCC => dec=5
    LPC_TIM1->TCR |= (1 << 1);			//reset van de timer

    NVIC_EnableIRQ(TIMER0_IRQn);		//irq-activatie
}

static void start_timer_1()
{
    LPC_TIM1->TCR= 1 << 0;            //aanzetten/enable van timer 1
}

static void stop_timer_1()
{
    LPC_TIM1->TCR |= (1 << 1);  	  //uitschakelen/disable van timer 1
}

static void start_timer_0()
{
    LPC_TIM0->TCR= 1 << 0;            //aanzetten/enable van timer 0
}

static void stop_timer_0()
{
    LPC_TIM0->TCR |= (1 << 1);         //uitschakelen/disable van timer 0
}

void dcc_transmitter_initialize()
{
	initialize_timer_1();
	initialize_timer_0();
    dcc_timer_transmitter_output_initialize();
    dcc_timer_transmitter_output_initialize_tracing();
}

struct dcc_transfer* transfers;

void dcc_transmitter_send(struct dcc_transfer transfers[],unsigned int size_in_bits)
{
	dcc_timer_transmitter_state_load_buffer(transfers,size_in_bits);
	start_timer_1();
}


void TIMER1_IRQHandler (void)
{
	timing_output_write_high();     //tracing on
	if((LPC_TIM1->IR & 0x01) == 0x01) {
		LPC_TIM1->IR |= 1 << 0;

	if (dcc_out==0){					//cases beneath will be executed here to prevent calculating delay

		dcc_output_write_low();

	}
	else
	{
		dcc_output_write_high();

	}

		if(dcc_timer_state_has_next()) {
			switch(dcc_timer_state_next()) {
				case DCC_TOGGLE_HIGH: /*dcc_output_write_high();*/	dcc_out=1;break;
				case DCC_TOGGLE_LOW:  /*dcc_output_write_low();	*/ 	dcc_out=0; break;
				case DCC_DO_NOTHING: 							break;
			}
		} else {
			//snelle repeat

			dcc_timer_transmitter_state_reset();
			//////////////////////////////////////

			//start_timer_0();
			//stop_timer_1();
		}
	}
	timing_output_write_low();		//tracing off
}

void TIMER0_IRQHandler (void)
{
    if((LPC_TIM0->IR & 0x01) == 0x01) {

    	printf("hello world! /n");

        dcc_timer_transmitter_state_reset();
        stop_timer_0(); //stop timer 2
        start_timer_1();//stop timer 1
    }
}
