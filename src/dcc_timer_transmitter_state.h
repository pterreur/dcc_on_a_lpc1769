/*
 * dcc_timer_state.h
 *
 *  Created on: Apr 1, 2016
 *      Author: bart
 */

#ifndef DCC_TIMER_TRANSMITTER_STATE_H_
#define DCC_TIMER_TRANSMITTER_STATE_H_

#include "dcc_transmitter.h"

enum dcc_timer_action {
	DCC_TOGGLE_HIGH,DCC_TOGGLE_LOW,DCC_DO_NOTHING
};

typedef void (*write_bit)(void);

void dcc_timer_transmitter_state_load_buffer(struct dcc_transfer transfers[],unsigned int size);

void dcc_timer_transmitter_state_reset();

unsigned char dcc_timer_state_has_next();

enum dcc_timer_action dcc_timer_state_next();

#endif /* DCC_TIMER_TRANSMITTER_STATE_H_ */
