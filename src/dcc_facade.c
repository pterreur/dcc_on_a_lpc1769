#include "dcc_facade.h"
#include "dcc_communication.h"

unsigned char speed_traps[] = {
		0x02,0x12,0x03,0x13,0x04,0x14,0x05,0x15,0x06,0x16,
		0x07,0x17,0x08,0X18,0x09,0x19,0x0a,0x1a,0x0b,0x1b,
		0x0c,0x1c,0x0d,0x1d,0x0e,0x1e,0x0f,0x1f //TODO: complete the list of speedtraps -- OK
		};

#define MAX_SPEED_TRAP		sizeof(speed_traps)/sizeof(unsigned char) + 1
#define BASE_INSTRUCTION	0x40

//TODO add other dcc-high-level-functions
int dcc_facade_set_speed(unsigned char adress,unsigned char speed_trap, unsigned char direction)
{
	unsigned char speed_instruction;

	//TODO is this algorithm correct? -- Seems ok!
	if (speed_trap <= MAX_SPEED_TRAP) {
		speed_instruction = speed_traps[speed_trap-1] | BASE_INSTRUCTION;
		if (direction == 0) {
			speed_instruction &= ~0x20;
		}
		else
		{
			speed_instruction |= 0x20;
		}
		dcc_communication_send_instruction_byte(adress,speed_instruction);
		return 0;

	}

	return -1;
}

//int dcc_facade_set_direction (unsigned char adress, unsigned char direction)
//{
//	unsigned char direction_instruction;
//
//	if ((BASE_INSTRUCTION == 1<<5) != direction) {
//		direction_instruction = (BASE_INSTRUCTION ^ 1<<5);				//toggle bit 5
//		dcc_communication_send_instruction_byte(adress,direction_instruction);
//		return 0;
//	}
//	return -1;
//}

//int dcc_facade_clear_light(unsigned char adress)
//{
//	BASE_INSTRUCTION &= 0<<0x04;
//	dcc_communication_send_instruction_byte(adress, );
//}
//
//int dcc_facade_set_light(unsigned char adress)
//{
//	BASE_INSTRUCTION |= 0<<0x04;
//	dcc_communication_send_instruction_byte(adress, );
//}
//
//int dcc_facade_set_sound(unsigned char adress)
//{
//
//}
