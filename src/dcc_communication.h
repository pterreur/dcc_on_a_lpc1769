
#ifndef DCC_COMMUNICATION_H_
#define DCC_COMMUNICATION_H_

int dcc_communication_send_instruction_byte(unsigned char adress,unsigned char instruction);

int dcc_communication_send_instruction_buffer(unsigned char adress,unsigned char* instruction,unsigned int instruction_size_in_bits);

#endif
