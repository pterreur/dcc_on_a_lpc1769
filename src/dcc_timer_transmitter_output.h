#ifndef DCC_TIMER_TRANSMITTER_OUTPUT_H_
#define DCC_TIMER_TRANSMITTER_OUTPUT_H_

void dcc_timer_transmitter_output_initialize();

void dcc_output_write_high();

void dcc_output_write_low();

#endif /* DCC_TIMER_TRANSMITTER_OUTPUT_H_ */
