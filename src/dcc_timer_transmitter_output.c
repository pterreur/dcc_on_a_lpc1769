
//#define MBED_CODE

#include "LPC17xx.h"
#include "dcc_timer_transmitter_output.h"

#define PCONP				(*((uint32_t *)(0x400FC0C4)))

#ifndef MBED_CODE     			//0.22 => setup LPCXPRESSO
	#include <cr_section_macros.h>

	#define PINSEL_POS		12
	#define PINSEL			(*((uint32_t *)(0x4002c004)))

	#define FIOPOS			22
	#define FIODIR			(*((uint32_t *)(0x2009c000)))

	#define FIOSET			(*((uint32_t *)(0x2009c018)))
	#define FIOCLR			(*((uint32_t *)(0x2009c01c)))

	#define FIOPIN			(*((uint32_t *)(0x2009C014)))
#else              			//1.29 => setup MBED
	#define PINSEL_POS		26
	#define PINSEL			(*((uint32_t *)(0x4002c00c)))

	#define FIOPOS			20
	#define FIODIR			(*((uint32_t *)(0x2009c020)))

	#define FIOSET			(*((uint32_t *)(0x2009c038)))
	#define FIOCLR			(*((uint32_t *)(0x2009c03c)))

	#define FIOPIN			(*((uint32_t *)(0x2009C034)))
#endif

void dcc_timer_transmitter_output_initialize()
{
    PCONP |= ( 1 << 15 ); 			//aanzetten van gpio's
    PINSEL &= (~(3 << PINSEL_POS));		//functie van pin bepalen

    FIODIR |= (1 << FIOPOS);
}


void dcc_output_write_high(){
	FIOPIN = FIOPIN  | (1 << FIOPOS);
}

void dcc_output_write_low() {
	FIOPIN = FIOPIN & ~(1 << FIOPOS);
}
